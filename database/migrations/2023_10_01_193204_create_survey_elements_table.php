<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('survey_elements', function (Blueprint $table) {
            $table->id();
            $table->foreignId('survey_page_id')->constrained();
            $table->string('title');
            $table->boolean('is_required')->default(0);
            $table->enum('type', ['text', 'radiogroup', 'checkbox']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('survey_elements');
    }
};
