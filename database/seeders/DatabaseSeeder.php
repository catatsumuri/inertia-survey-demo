<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // パーミッションの作成
        $editArticlesPermission = Permission::create(['name' => 'edit articles']);

        // ロールの作成
        $adminRole = Role::create(['name' => 'admin']);

        // ロールにパーミッションを付与
        $adminRole->givePermissionTo($editArticlesPermission);

        // admin1 ユーザーの作成
        $admin1 = User::factory()->create([
            'name' => 'Admin 1',
            'email' => 'admin1@example.com',
            'password' => bcrypt('password'),
        ]);

        // admin2 ユーザーの作成
        $admin2 = User::factory()->create([
            'name' => 'Admin 2',
            'email' => 'admin2@example.com',
            'password' => bcrypt('password'),
        ]);

        // ユーザーにadminロールを割り当て
        $admin1->assignRole('admin');
        $admin2->assignRole('admin');

        // 一般ユーザー
        $user1 = User::factory()->create([
            'name' => 'User 1',
            'email' => 'user1@example.com',
            'password' => bcrypt('password'),
        ]);

        // User::factory(50)->create();
        $this->call([
            SurveyQuestionSeeder::class,
            SurveySeeder::class,
        ]);
    }
}
