<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

use App\Models\SurveyQuestion;
class SurveyQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $files = File::glob(database_path('seeders/data/*.json'));

        foreach ($files as $file) {
            $jsonString = File::get($file);
            $questionData = json_decode($jsonString, true);
            $name = $questionData['title'] ?? null;
            if (!$name) {
                $name = '質問セット_'. now()->format('YmdHis');
            }
            $fileName = pathinfo($file, PATHINFO_FILENAME);
            $type = Str::camel(str_replace('_', ' ', $fileName));
            SurveyQuestion::factory()->create([
                'name'          => $name,
                'type'          => $type,
                'question_data' => $questionData,
            ]);
        }
    }
}
