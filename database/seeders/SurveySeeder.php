<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Survey;
use App\Models\SurveyQuestion;
use App\Services\SurveyService;

class SurveySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(SurveyService $surveyService): void
    {
        $surveyQuestion = SurveyQuestion::first();
        $survey = Survey::factory()->create([
            'title'       => 'ダミーの質問',
            'description' => 'ダミーの質問です',
            'type'        => $surveyQuestion->type,
        ]);
        $surveyStructure = $surveyQuestion->question_data;
        $surveyService->createSurveyPage($survey, $surveyStructure);
    }
}
