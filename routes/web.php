<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SurveyController;
use App\Http\Controllers\SurveyTakingController;
use App\Http\Controllers\SurveyResponseController;
use App\Http\Controllers\DashboardController;

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

use App\Http\Middleware\EnsureUserIsAdmin;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::group(['middleware' => ['auth', 'verified']], function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('surveys/{survey}/take', [SurveyTakingController::class, 'show'])->name('surveys.take');
    Route::resource('surveys.responses', SurveyResponseController::class);
    Route::get('surveys/{survey}/responses/{response}/aggregate', [SurveyResponseController::class, 'aggregate'])->name('surveys.responses.aggregate');
});

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    // Admin route
    Route::prefix('admin')->middleware(['auth', EnsureUserIsAdmin::class])->group(function () {
        Route::resource('users', UserController::class);
        Route::get('users/{user}/activity-log', [UserController::class, 'showActivityLog'])->name('users.activity-log');

        Route::resource('surveys', SurveyController::class);
        Route::post('surveys/{survey}/preview', [SurveyController::class, 'previewStore'])->name('surveys.previewStore');
        Route::delete('surveys/{survey}/preview', [SurveyController::class, 'previewDestroy'])->name('surveys.previewDestroy');
    });
});

require __DIR__.'/auth.php';
