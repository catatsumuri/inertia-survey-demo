<?php
use Diglactic\Breadcrumbs\Breadcrumbs;
use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;

use App\Models\User;
use App\Models\Survey;
use App\Models\SurveyResponse;

Breadcrumbs::for('dashboard', function(BreadcrumbTrail $trail)
{
    $trail->push(__('Dashboard'), route('dashboard'));
});
Breadcrumbs::for('surveys.take', function(BreadcrumbTrail $trail, Survey $survey)
{
    $trail->parent('dashboard');
    $trail->push($survey->title, route('surveys.take', $survey));
});
Breadcrumbs::for('surveys.responses.show', function(BreadcrumbTrail $trail, Survey $survey, SurveyResponse $response)
{
    $trail->parent('dashboard');
    $trail->push($survey->title, route('surveys.responses.show', [$survey, $response]));
});
Breadcrumbs::for('surveys.responses.aggregate', function(BreadcrumbTrail $trail, Survey $survey, SurveyResponse $response)
{
    $trail->parent('dashboard');
    $trail->push($survey->title, route('surveys.responses.aggregate', [$survey, $response]));
});

Breadcrumbs::for('users.index', function(BreadcrumbTrail $trail)
{
    $trail->push(__('Users'), route('users.index'));
});
Breadcrumbs::for('users.create', function(BreadcrumbTrail $trail)
{
    $trail->parent('users.index');
    $trail->push(__('Create'), route('users.create'));
});
Breadcrumbs::for('users.show', function (BreadcrumbTrail $trail, User $user) {
    $trail->parent('users.index');
    $trail->push($user->name ?? 'Unnamed', route('users.show', $user));
});
Breadcrumbs::for('users.edit', function (BreadcrumbTrail $trail, User $user) {
    $trail->parent('users.show', $user);
    $trail->push(__('Update User'), route('users.edit', $user));
});
Breadcrumbs::for('users.activity-log', function (BreadcrumbTrail $trail, User $user) {
    $trail->parent('users.show', $user);
    $trail->push(__('Activity'), route('users.activity-log', $user));
});


Breadcrumbs::for('surveys.index', function(BreadcrumbTrail $trail)
{
    $trail->push(__('Surveys'), route('surveys.index'));
});
Breadcrumbs::for('surveys.create', function(BreadcrumbTrail $trail)
{
    $trail->parent('surveys.index');
    $trail->push(__('Create'), route('surveys.create'));
});
Breadcrumbs::for('surveys.show', function(BreadcrumbTrail $trail, Survey $survey)
{
    $trail->parent('surveys.index');
    $trail->push(__('Preview'), route('surveys.show', $survey));
});
Breadcrumbs::for('surveys.edit', function(BreadcrumbTrail $trail, Survey $survey)
{
    $trail->parent('surveys.index');
    $trail->push(__('Settings'), route('surveys.edit', $survey));
});
