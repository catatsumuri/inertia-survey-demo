<?php
namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Inertia\Testing\AssertableInertia as Assert;

use App\Models\User;
class UserRequestTest extends TestCase
{
    use RefreshDatabase;
    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('db:seed');
    }


    public function test_store_empty_value_not_accepted(): void
    {
        // Adminユーザーを作成
        $admin = User::factory()->create();
        $admin->assignRole('admin');
        $this->actingAs($admin);

        $response = $this->post(route('users.index'), []);

        // バリデーションエラーがあるため、302ステータスコードが返されることを確認
        $response->assertStatus(302);

        // セッションにエラーが存在することを確認
        $response->assertSessionHasErrors(['name', 'email', 'password']);
    }

    public function test_update_success(): Void
    {
        $admin = User::factory()->create();
        $admin->assignRole('admin');
        $this->actingAs($admin);

        // テストユーザーを作成
        $user = User::factory()->create();

        // 更新データ
        $updatedData = [
            'name' => 'Updated Name',
            'email' => 'updated.email@example.com',
            // 他の更新対象のデータも含める
        ];

        // ユーザー情報を更新するリクエストを送信
        $response = $this->put(route('users.update', ['user' => $user->id]), $updatedData);

        // リダイレクト先やステータスコードを確認
        $response->assertStatus(302)
                 ->assertRedirect(route('users.show', $user))
                 ->assertSessionDoesntHaveErrors();

        // データベースから更新後のユーザー情報を取得
        $updatedUser = User::find($user->id);

        // ユーザー情報が正しく更新されたことを確認
        $this->assertEquals($updatedData['name'], $updatedUser->name);
        $this->assertEquals($updatedData['email'], $updatedUser->email);
        // 他の更新対象のデータも確認

        $response = $this->get(route('users.index'));
        $response->assertInertia(fn (Assert $page) => $page
                 ->component('Users/Index')
                 ->has('users')
        );
    }

    public function test_update_unique_email(): void
    {
        // 既存のユーザーを2つ作成
        $existingUser1 = User::factory()->create();
        $existingUser2 = User::factory()->create();

        $admin = User::factory()->create();
        $admin->assignRole('admin');
        $this->actingAs($admin);

        // テストユーザーを作成
        $user = User::factory()->create();

        // 更新データ（ユニークなメールアドレスを既存のユーザーと同じにする）
        $updatedData = [
            'name' => 'Updated Name',
            'email' => $existingUser1->email, // 既存のユーザーのメールアドレスと同じにする
            // 他の更新対象のデータも含める
        ];

        // ユーザー情報を更新するリクエストを送信
        $response = $this->put(route('users.update', ['user' => $user->id]), $updatedData);

        // バリデーションエラーがあることを確認
        $response->assertSessionHasErrors(['email']);

        // データベースから更新後のユーザー情報を再取得
        $updatedUser = User::find($user->id);

        // ユーザー情報が正しく更新されていないことを確認
        $this->assertNotEquals($updatedData['email'], $updatedUser->email);
    }


}
