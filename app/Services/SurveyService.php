<?php
namespace App\Services;

use App\Models\Survey;
use Illuminate\Support\Collection;
use App\Models\SurveyPage;
use App\Models\SurveyElement;
use App\Models\SurveyElementChoice;

class SurveyService
{
    public function getSurveyData(Survey $survey): string
    {
        $pagesData = $this->getPagesData($survey);
        $settings = $survey->settings;
        $surveyData = [
            'pages' => $pagesData,
        ];
        if ($settings) {
            $surveyData = array_merge($surveyData, $settings);
        }
        $surveyData = json_encode($surveyData, JSON_UNESCAPED_UNICODE);
        return $surveyData;
    }

    public function getPagesData(Survey $survey): Collection
    {
        $pages = $survey->pages()->with(['elements.choices'])->get();
        $pagesData = $pages->map(function ($page) {
            $elements = $page->elements->map(function ($element) {
                $choices =  $element->choices->map(function ($choice) {
                    return [
                        'value' => $choice->id,
                        'text'  => $choice->choice,
                    ];
                })->all();

                return [
                    'type'       => $element->type,
                    'name'       => (string)$element->id,
                    'title'      => $element->title,
                    'isRequired' => $element->is_required,
                    'choices'    => $choices,
                ];
            })->all();

            return [
                'name'     => $page->name,
                'elements' => $elements,
            ];
        });
        return $pagesData;
    }

    public function getPreviewResponseData(Survey $survey)
    {
        $surveyData = array_reduce($responseDetails->toArray(), function ($carry, $detail) {
            $responseValue = $detail['response_value'];
            // $responseValueが配列であり、'value'キーが存在する場合
            if (is_array($responseValue) && isset($responseValue['value'])) {
                $value = $responseValue['value'];  // 'value'キーの値を取り出す
            } else {
                $value = $responseValue;  // それ以外の場合、$responseValueをそのまま使用
            }
            $carry[$detail['survey_element_id']] = $value;
            return $carry;
        }, []);
    }

    public function getSettingsMetadata(): array
    {
        return [
            'questionsOnPageMode' => [
                'type' => 'select',
                'options' => ['singlePage', 'questionPerPage', 'standard'],
                'default' => 'standard',
                'label' => __('Question On Page Mode'),
            ],
            'showQuestionNumbers' => [
                'type' => 'select',
                'options' => ['on', 'onpage', 'off'],
                'default' => 'on',
                'label' => __('Show Question Numbers'),
            ],
            'showCompletedPage' => [
                'type' => 'select',
                'options' => ['True', 'False'],
                'default' => 'True',
                'label' => __('Show Completed Page'),
            ],
        ];
    }

    public function createSurveyPage(Survey $survey, array $surveyStructure): void
    {
        foreach ($surveyStructure['pages'] as $page) {
            $data = [
                'name'      => $page['name'],
                'survey_id' => $survey->id,
            ];
            $surveyPage = SurveyPage::create($data);

            foreach ($page['elements'] as $element) {
                $data                 = [
                    'survey_page_id' => $surveyPage->id,
                    'type'           => $element['type'],
                    'title'          => $element['title'],
                    'is_required'    => $element['isRequired'] ?? false,
                ];
                $surveyElement = SurveyElement::create($data);

                if ($choices = $element['choices'] ?? null) {
                    foreach ($choices as $choice) {
                        $data = [
                            'survey_element_id' => $surveyElement->id,
                            'choice'            => $choice,
                        ];
                        SurveyElementChoice::create($data);
                    }
                }
            }
        }
    }
}
