<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

use App\Models\Survey;

use Inertia\Inertia;
use Inertia\Response;
use App\Services\SurveyService;


class SurveyTakingController extends Controller
{
    public function show(Survey $survey, SurveyService $surveyService): Response
    {
        $surveyData = $surveyService->getSurveyData($survey);

        return Inertia::render('Surveys/Take', [
            'surveyModel'   => $survey,
            'surveyData'    => $surveyData,
        ]);
    }
}
