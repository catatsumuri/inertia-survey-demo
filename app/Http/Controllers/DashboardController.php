<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;
use App\Models\Survey;

class DashboardController extends Controller
{
    public function index(): Response
    {
        $surveys = Survey::latest()->get()->map(function ($survey) {
            $response = $survey->responses()->where('user_id', auth()->id())->first();
            $survey->response_id = $response ? $response->id : null;
            $survey->response_created_at = $response ? $response->created_at : null;
            return $survey;
        });

        return Inertia::render('Dashboard', [
            'surveys' => $surveys
        ]);
    }
}
