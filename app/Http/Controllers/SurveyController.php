<?php

namespace App\Http\Controllers;

use App\Models\Survey;
use App\Models\SurveyQuestion;
use App\Models\SurveyPage;
use App\Models\SurveyElement;
use App\Models\SurveyElementChoice;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use Inertia\Inertia;
use Inertia\Response;

use App\Http\Requests\SurveyRequest;

use App\Services\SurveyService;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): Response
    {
        $surveys = Survey::latest()->get();
        $userId = auth()->id();

        return Inertia::render('Surveys/Index', ['surveys' => $surveys]);
    }

    /*
        $surveys = Survey::latest()->get()->each(function ($survey) use ($userId) {
            $survey->hasResponse = $survey->hasResponseFromUser($userId);
        });
     */

    /**
     * Show the form for creating a new resource.
     */
    public function create(): Response
    {
        $surveyQuestionSets = SurveyQuestion::pluck('name', 'id');
        return Inertia::render('Surveys/Create', [
            'surveyQuestionSets' => $surveyQuestionSets,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(SurveyRequest $request, SurveyService $surveyService): RedirectResponse
    {
        $data = $request->validated();
        $data['settings'] = [];
        $surveyQuestionSetId = $request->survey_question_set_id;
        $surveyQuestion = SurveyQuestion::findOrFail($surveyQuestionSetId);
        $surveyStructure = $surveyQuestion->question_data;
        $data['type'] = $surveyQuestion->type;

        DB::beginTransaction();
        $survey = Survey::create($data);
        $surveyService->createSurveyPage($survey, $surveyStructure);
        DB::commit();
        return redirect(route('surveys.index'))
            ->with(['success' => __('New Survey Created')])
        ;
    }

    /**
     * Preview the survey
     */
    public function show(Request $request, Survey $survey, SurveyService $surveyService): Response
    {
        $surveyData = $surveyService->getSurveyData($survey);

        $responseCount = 0;
        if ($previewData = $request->session()->get('preview_data')) {
            $responseCount = count($previewData);
        }

        return Inertia::render('Surveys/Show', [
            'surveyModel'  => $survey,
            'surveyData'   => $surveyData,
            'responseData' => $previewData,
            'readOnly'     => $responseCount ? true : false,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Survey $survey, SurveyService $surveyService): Response
    {
        $surveyMetaData = $surveyService->getSettingsMetadata();
        return Inertia::render('Surveys/Edit', [
            'survey'         => $survey,
            'surveyMetaData' => $surveyMetaData,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(SurveyRequest $request, Survey $survey): RedirectResponse
    {
        $data = $request->validated();
        $survey->update($data);
        return redirect(route('surveys.index', $survey))
            ->with(['success' => __('Survey Settings Updated')]);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Survey $survey)
    {
        //
    }

    public function previewStore(Request $request, Survey $survey): RedirectResponse
    {
        $request->session()->put('preview_data', $request->all());

        return redirect(route('surveys.show', $survey))
            ->with(['success' => __('Preview data received')]);
    }
    public function previewDestroy(Request $request, Survey $survey)
    {
        $request->session()->forget('preview_data');

        return redirect(route('surveys.show', $survey))
            ->with(['success' => __('Preview data destroyed')]);
    }
}
