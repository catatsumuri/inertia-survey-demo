<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

use Inertia\Inertia;
use Inertia\Response;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): Response
    {
        // $users = User::with('roles')->get();
        $users = User::with('roles')->orderBy('id', 'asc')->paginate(20);

        return Inertia::render('Users/Index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): Response
    {
        return Inertia::render('Users/Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(UserRequest $request): RedirectResponse
    {
        $data = $request->validated();
        $data['email_verified_at'] = now();
        $data['password'] = bcrypt($data['password']);

        User::create($data);

        return redirect(route('users.index'))
            ->with('success', __('New User Created'))
        ;
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user): Response
    {
        // dd($roleNames = $user->getRoleNames());
        // dd(\Spatie\Permission\Models\Role::all());
        $user->load(['roles', 'activities']);
        return Inertia::render('Users/Show', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user): Response
    {
        return Inertia::render('Users/Edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UserRequest $request, User $user): RedirectResponse
    {
        $data = $request->validated();
        if (array_key_exists('password', $data) && $data['password']) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        $user->update($data);

        return redirect(route('users.show', $user))
            ->with('success', __('User Updated'))
        ;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, User $user): RedirectResponse
    {
        $user->delete();
        return redirect(route('users.index'))
            ->with('success', __('User Deleted'))
        ;
    }

    /**
     * Show user's activity log
     */
    public function showActivityLog(User $user): Response
    {
        $activities = $user->activities()->orderBy('created_at', 'desc')->paginate(20);
        $activities = $activities->toArray();

        return Inertia::render('Users/ActivityLog', [
            'user' => $user,
            'activities' => $activities,
        ]);

        /*
        $user->load(['activities' => function ($query) {
            $query->orderBy('created_at', 'desc');
        }]);
        return Inertia::render('Users/ActivityLog', ['user' => $user]);
         */
    }
}
