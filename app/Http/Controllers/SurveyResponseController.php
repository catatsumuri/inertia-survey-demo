<?php

namespace App\Http\Controllers;

use App\Models\Survey;
use App\Models\SurveyResponse;
use App\Models\SurveyResponseDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Inertia\Inertia;
use Inertia\Response;
use App\Services\SurveyService;

class SurveyResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Survey $survey)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Survey $survey)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, Survey $survey)
    {
        DB::beginTransaction();

        $surveyResponse = SurveyResponse::create([
            'user_id'   => $request->user()->id,
            'survey_id' => $survey->id
        ]);
        foreach ($request->all() as $elementId => $responseValue) {
            if (!is_array($responseValue)) {
                $responseValue = ['value' => $responseValue];
            }
            $data = [
                'survey_response_id' => $surveyResponse->id,
                'survey_element_id'  => $elementId,
                'response_value'     => $responseValue,
            ];
            SurveyResponseDetail::create($data);
        }
        DB::commit();

        return redirect(route('dashboard'))
            ->with(['success' => '保存しました'])
        ;
    }

    /**
     * Display the specified resource.
     */
    public function show(Survey $survey, SurveyResponse $response, SurveyService $surveyService)
    {
        $surveyData  = $surveyService->getSurveyData($survey);
        $survey->response_id = $response->id;
        $previewData = $survey->getUserResponseData(auth()->id());
        $component = $survey->type;
        $componentPath = resource_path('js/Components/AggregateViews/') . $component . '.jsx';
        $isComponentAvailable = file_exists($componentPath);

        return Inertia::render('SurveyResponses/Show', [
            'surveyModel'          => $survey,
            'surveyData'           => $surveyData,
            'responseData'         => $previewData,
            'isComponentAvailable' => $isComponentAvailable,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Survey $survey, SurveyResponse $surveyResponse)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Survey $survey, SurveyResponse $surveyResponse)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Survey $survey, SurveyResponse $surveyResponse)
    {
        //
    }

    public function aggregate(Survey $survey, SurveyResponse $response, SurveyService $surveyService)
    {
        $surveyData  = $surveyService->getSurveyData($survey);
        $survey->response_id = $response->id;
        $previewData = $survey->getUserResponseData(auth()->id());
        $currentComponent = $survey->type;

        return Inertia::render('SurveyResponses/Aggregate', [
            'surveyModel' => $survey,
            'surveyData'  => $surveyData,
            'component'   => $currentComponent,
        ]);
    }

}
