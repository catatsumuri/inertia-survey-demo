<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SurveyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules =  [
            'title'                  => 'required|string|max:255',
            'description'            => 'nullable|string',
            'survey_question_set_id' => 'required|integer|exists:survey_questions,id',
            'settings'               => 'nullable|array',
        ];

        if ($this->isMethod('patch') || $this->isMethod('put')) {
            unset($rules['survey_question_set_id']);
        }

        return $rules;
    }
}
