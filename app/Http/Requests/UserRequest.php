<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules;
use Illuminate\Validation\Rule;
use App\Models\User;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $rules = [
            'name'  => 'required|string|max:255',
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
            ],
            'password' => [
                'nullable',
                'confirmed',
                Rules\Password::defaults()
            ],
        ];
        if ($this->isMethod('patch') || $this->isMethod('put')) {
            // If update
            $userId = $this->route('user')->id;
            // あるいはtype hintより
            // $userId = $this->user->id;
            $rules['email'][] = Rule::unique('users')->ignore($userId);
        } else {
            // If store
            $rules['email'][] = 'unique:users';
            $rules['password'][] = 'required';
        }

        return $rules;
    }
}
