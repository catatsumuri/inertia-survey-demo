<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Verified;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

use App\Notifications\NewUserEmailVerifiedNotification;
use App\Models\User;

class SendEmailToAdminsWhenVerified
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(Verified $event): void
    {
        $verifiedUser = $event->user;
        $adminUsers = User::role('admin')->get();
        Notification::send($adminUsers, new NewUserEmailVerifiedNotification($verifiedUser));
    }
}
