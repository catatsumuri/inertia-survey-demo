<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

use App\Notifications\NewUserRegisteredNotification;
use App\Models\User;

class SendEmailToAdminsWhenRegistered
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(Registered $event): void
    {
        $newUser = $event->user;
        $adminUsers = User::role('admin')->get();
        Notification::send($adminUsers, new NewUserRegisteredNotification($newUser));
    }
}
