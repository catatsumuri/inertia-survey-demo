<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SurveyElementChoice extends Model
{
    use HasFactory;

    protected $fillable = [
        'survey_element_id',
        'choice',
    ];

    public function SurveyElement(): BelongsTo
    {
        return $this->hasMany(SurveyElement::class);
    }
}
