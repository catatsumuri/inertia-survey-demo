<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Survey extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'settings',
        'type',
    ];

    protected $casts = [
        'settings' => 'array',
    ];

    public function pages(): HasMany
    {
        return $this->hasMany(SurveyPage::class);
    }

    public function responses(): HasMany
    {
        return $this->hasMany(SurveyResponse::class);
    }

    public function getUserResponseData($userId): array
    {
        $responseCount = $this->responses()->where('user_id', $userId)->count();
        $responseData = [];
        if ($responseCount) {
            $responseDetails = $this->responses()
                                    ->with('details.element')
                                    ->where('user_id', $userId)
                                    ->first()
                                    ->details;
            $responseData = array_reduce($responseDetails->toArray(), function ($carry, $detail) {
                $responseValue = $detail['response_value'];
                // $responseValueが配列であり、'value'キーが存在する場合
                if (is_array($responseValue) && isset($responseValue['value'])) {
                    $value = $responseValue['value']; // 'value'キーの値を取り出す
                } else {
                    $value = $responseValue; // それ以外の場合、$responseValueをそのまま使用
                }
                $carry[$detail['survey_element_id']] = $value;
                return $carry;
            }, []);
        }

        return $responseData;
    }
}
