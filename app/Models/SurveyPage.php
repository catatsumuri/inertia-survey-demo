<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class SurveyPage extends Model
{
    use HasFactory;
    protected $fillable = [
        'survey_id',
        'name',
    ];

    public function elements(): HasMany
    {
        return $this->hasMany(SurveyElement::class);
    }
}
