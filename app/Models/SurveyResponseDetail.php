<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SurveyResponseDetail extends Model
{
    use HasFactory;

    protected $casts = [
        'response_value' => 'array',
    ];

    protected $fillable = [
        'survey_response_id',
        'survey_element_id',
        'response_value',
    ];

    public function element(): BelongsTo
    {
        return $this->belongsTo(SurveyElement::class, 'survey_element_id');
    }
}
