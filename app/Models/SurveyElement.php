<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class SurveyElement extends Model
{
    use HasFactory;

    protected $fillable = [
        'survey_page_id',
        'type',
        'title',
        'is_required',
    ];

    public function choices(): HasMany
    {
        return $this->hasMany(SurveyElementChoice::class);
    }
}
