// useConfiguredDayjs.js
import { useLaravelReactI18n } from 'laravel-react-i18n';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import 'dayjs/locale/ja';

export const useConfiguredDayjs = () => {
    const { currentLocale } = useLaravelReactI18n();
    dayjs.extend(relativeTime);
    const locale = currentLocale();
    if (locale) dayjs.locale(locale);

    return dayjs;
}
