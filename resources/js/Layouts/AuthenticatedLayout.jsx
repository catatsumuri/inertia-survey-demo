import { useState, useEffect } from 'react';
import { Link, usePage } from '@inertiajs/react';
import { useLaravelReactI18n } from 'laravel-react-i18n';

import ResponsiveNavLink from '@/Components/ResponsiveNavLink';
import ApplicationLogo from '@/Components/ApplicationLogo';
import Dropdown from '@/Components/Dropdown';
import NavLink from '@/Components/NavLink';

// Additional
import Breadcrumbs from '@/Components/Breadcrumbs';

import 'react-toastify/dist/ReactToastify.css';
import { toast, ToastContainer } from 'react-toastify';
import {
  VscChevronDown,
  VscMenu,
  VscClose,
  VscDashboard,
  VscOrganization,
  VscNote,
  VscIndent,
  VscOutput,
} from 'react-icons/vsc';

export default function Authenticated({ user, header, children }) {
  const { t } = useLaravelReactI18n();
  const { props: { auth, flash } } = usePage();

  useEffect(() => {
    if (flash.success) {
      toast.success(t(flash.success));
    }
    if (flash.error) {
      toast.error(t(flash.error));
    }
  }, [flash]);

  const [showingNavigationDropdown, setShowingNavigationDropdown] = useState(false);

  return (
    <div className="min-h-screen bg-gray-100">
      <nav className="bg-white border-b border-gray-100">
        <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
          <div className="flex justify-between h-16">
            <div className="flex">
              <div className="shrink-0 flex items-center">
                <Link href="/">
                  <ApplicationLogo className="block h-9 w-auto fill-current text-gray-800" />
                </Link>
              </div>

              <div className="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                <NavLink href={route('dashboard')} active={route().current('dashboard')}>
                  <VscDashboard className="mr-2" />
                  {t('Dashboard')}
                </NavLink>
                {auth.isAdmin && (
                  <>
                    <NavLink href={route('users.index')} active={route().current('users.*')}>
                      <VscOrganization className="mr-2" />
                      {t('Users')}
                    </NavLink>
                    <NavLink href={route('surveys.index')} active={route().current('surveys.*')}>
                      <VscOutput className="mr-2" />
                      {t('Surveys')}
                    </NavLink>
                  </>
                )}
              </div>
            </div>

            <div className="hidden sm:flex sm:items-center sm:ml-6">
              <div className="ml-3 relative">
                <Dropdown>
                  <Dropdown.Trigger>
                    <span className="inline-flex rounded-md">
                      <button
                        type="button"
                        className="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:text-gray-700 focus:outline-none transition ease-in-out duration-150"
                      >
                        {user.name}
                        <VscChevronDown className="ml-2" />

                      </button>
                    </span>
                  </Dropdown.Trigger>

                  <Dropdown.Content>
                    <Dropdown.Link href={route('profile.edit')}>{t('Profile')}</Dropdown.Link>
                    <Dropdown.Link href={route('logout')} method="post" as="button">
                      {t('Log Out')}
                    </Dropdown.Link>
                  </Dropdown.Content>
                </Dropdown>
              </div>
            </div>

            <div className="-mr-2 flex items-center sm:hidden">
              <button
                type="button"
                onClick={() => setShowingNavigationDropdown((previousState) => !previousState)}
                className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out"
              >
                <VscMenu className={!showingNavigationDropdown ? 'inline-flex' : 'hidden'} />
                <VscClose className={showingNavigationDropdown ? 'inline-flex' : 'hidden'} />
              </button>
            </div>
          </div>
        </div>

        <div className={`${showingNavigationDropdown ? 'block' : 'hidden'} sm:hidden`}>
          <div className="pt-2 pb-3 space-y-1">
            <ResponsiveNavLink href={route('dashboard')} active={route().current('dashboard')}>
              <VscDashboard className="mr-2" />
              {t('Dashboard')}
            </ResponsiveNavLink>

            {auth.isAdmin && (
              <>
                <ResponsiveNavLink href={route('users.index')} active={route().current('users.*')}>
                  <VscOrganization className="mr-2" />
                  {t('Users')}
                </ResponsiveNavLink>
                <ResponsiveNavLink href={route('surveys.index')} active={route().current('surveys.*')}>
                  <VscOutput className="mr-2" />
                  {t('Surveys')}
                </ResponsiveNavLink>
              </>
            )}
          </div>

          <div className="pt-4 pb-1 border-t border-gray-200 bg-gray-100">
            <div className="px-4">
              <div className="font-medium text-base text-gray-800">{user.name}</div>
              <div className="font-medium text-sm text-gray-500">{user.email}</div>
            </div>

            <div className="mt-3 space-y-1">
              <ResponsiveNavLink href={route('profile.edit')}>
                <VscNote className="mr-2" />
                {' '}
                {t('Profile')}
              </ResponsiveNavLink>
              <ResponsiveNavLink method="post" href={route('logout')} as="button">
                <VscIndent className="mr-2" />
                {' '}
                {t('Log Out')}
              </ResponsiveNavLink>
            </div>
          </div>
        </div>
      </nav>

      {header && (
        <header className="bg-white shadow">
          <div className="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">{header}</div>
          <Breadcrumbs />
        </header>
      )}

      <ToastContainer />
      <main>{children}</main>
    </div>
  );
}
