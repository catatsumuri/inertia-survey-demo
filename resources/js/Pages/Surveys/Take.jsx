import React, { useCallback } from 'react';
import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head, router, usePage } from '@inertiajs/react';
import { Model } from 'survey-core';
import SurveyWrapper from '@/Components/SurveyWrapper';

export default function SurveyTake({
  auth, surveyModel, surveyData,
}) {
  const survey = new Model(surveyData);
  const { locale } = usePage().props;

  const surveyComplete = useCallback((sender) => {
    router.post(route('surveys.responses.store', surveyModel.id), sender.data);
  }, []);
  survey.onComplete.add(surveyComplete);

  return (
    <AuthenticatedLayout
      user={auth.user}
      header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">{surveyModel.title}</h2>}
    >
      <Head title={surveyModel.title} />

      <div className="py-12">
        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
          <SurveyWrapper model={survey} locale={locale} />
        </div>
      </div>

    </AuthenticatedLayout>
  );
}
