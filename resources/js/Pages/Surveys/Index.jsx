import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head } from '@inertiajs/react';
import { useLaravelReactI18n } from 'laravel-react-i18n';
import PrimaryButton from '@/Components/PrimaryButton';
import SecondaryButton from '@/Components/SecondaryButton';
import {
  VscSettingsGear,
  VscOpenPreview,
} from 'react-icons/vsc';

export default function SurveyIndex({ auth, surveys }) {
  const { t } = useLaravelReactI18n();

  return (
    <AuthenticatedLayout
      user={auth.user}
      header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">{t('Surveys')}</h2>}
    >
      <Head title={t('Surveys')} />

      <div className="py-12">
        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
          <div className="bg-white p-6 rounded shadow-md max-w-7xl mx-auto">
            <div className="flex justify-between items-center mb-4">
              <h3 className="text-2xl font-semibold">
                {t('Available Surveys')}
              </h3>
              <PrimaryButton href={route('surveys.create')}>
                {t('Create New Survey')}
              </PrimaryButton>
            </div>

            <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-4">
              {surveys.map((survey, index) => (
                <div key={index} className="bg-white rounded-lg shadow-lg p-4 border border-gray-300">
                  <div className="flex justify-between items-center mb-2">
                    <h4 className="text-lg font-semibold">
                      {survey.title}
                    </h4>

                    <SecondaryButton href={route('surveys.edit', survey.id)}>
                      <VscSettingsGear className="mr-2" /> {t('Settings')}
                    </SecondaryButton>
                  </div>

                  <p className="text-sm text-gray-700 mb-3">
                    {survey.description}
                  </p>

                  <PrimaryButton href={route('surveys.show', survey.id)}>
                    <VscOpenPreview className="mr-2" /> {t('Preview')}
                  </PrimaryButton>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </AuthenticatedLayout>
  );
}
