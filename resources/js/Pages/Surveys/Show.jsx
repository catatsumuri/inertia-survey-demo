import React, { useCallback } from 'react';
import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Model } from 'survey-core';
import { Head, router, usePage } from '@inertiajs/react';
import { VscTrash } from 'react-icons/vsc';
import { useLaravelReactI18n } from 'laravel-react-i18n';
import DangerButton from '@/Components/DangerButton';
import SurveyWrapper from '@/Components/SurveyWrapper';

export default function SurveyShow({
  auth, surveyModel, surveyData, responseData, readOnly,
}) {
  const { t } = useLaravelReactI18n();
  const survey = new Model(surveyData);
  const { locale } = usePage().props;

  const surveyComplete = useCallback((sender) => {
    router.post(route('surveys.previewStore', surveyModel.id), sender.data);
  }, []);
  survey.onComplete.add(surveyComplete);

  const handleDelete = () => {
    router.delete(route('surveys.previewDestroy', surveyModel.id));
  };

  return (
    <AuthenticatedLayout
      user={auth.user}
      header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">{surveyModel.title}</h2>}
    >
      <Head title={surveyModel.title} />

      <div className="pt-12">
        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
          {readOnly && (
          <div className="text-right">
            <DangerButton onClick={() => handleDelete()}>
              <VscTrash className="mr-2" /> {t('Delete Preview Data')}
            </DangerButton>
          </div>
          )}
        </div>
      </div>

      <div className="py-12">
        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
          <SurveyWrapper
            model={survey}
            readOnly={readOnly}
            responseData={responseData}
            locale={locale}
          />
        </div>
      </div>
    </AuthenticatedLayout>
  );
}
