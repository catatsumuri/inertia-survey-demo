import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head, useForm } from '@inertiajs/react';
import { useLaravelReactI18n } from 'laravel-react-i18n';

import InputError from '@/Components/InputError';
import InputLabel from '@/Components/InputLabel';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';
import SurveySelectSetting from '@/Components/SurveySelectSetting';

export default function SurveyEdit({ auth, survey, surveyMetaData }) {
  const { t } = useLaravelReactI18n();

  const {
    data, setData, put, errors, processing,
  } = useForm({
    title: survey.title,
    description: survey.description,
    settings: survey.settings,
  });

  const updateSetting = (key, value) => {
    setData('settings', {
      ...data.settings,
      [key]: value,
    });
  };

  const submit = (e) => {
    e.preventDefault();
    put(route('surveys.update', survey.id));
  };

  return (
    <AuthenticatedLayout
      user={auth.user}
      header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">{t('Settings')}</h2>}
    >
      <Head title={t('Settings')} />
      <div className="py-12">
        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
          <div className="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
            <form onSubmit={submit} className="mt-6 space-y-6">
              <div>
                <InputLabel htmlFor="title" value={t('Title')} />

                <TextInput
                  id="title"
                  className="mt-1 block w-full"
                  value={data.title}
                  onChange={(e) => setData('title', e.target.value)}
                  onSubmit={submit}
                />

                <InputError className="mt-2" message={errors.title} />
              </div>

              <div className="mt-3">
                <InputLabel htmlFor="description" value={t('Description')} />

                <TextInput
                  id="description"
                  className="mt-1 block w-full"
                  value={data.description}
                  onChange={(e) => setData('description', e.target.value)}
                  onSubmit={submit}
                />

                <InputError className="mt-2" message={errors.description} />
              </div>

              {Object.entries(surveyMetaData).map(([settingKey, metaData]) => {
                const {
                  type, options, default: defaultValue, label,
                } = metaData;
                switch (type) {
                  case 'select':
                    return (
                      <div className="mt-3" key={settingKey}>
                        <SurveySelectSetting
                          settingKey={settingKey}
                          value={data.settings?.[settingKey] || defaultValue}
                          onChange={updateSetting}
                          options={options}
                          label={label}
                        />
                      </div>
                    );
                  default:
                    return null;
                }
              })}

              <div className="flex items-center gap-4">
                <PrimaryButton disabled={processing}>{t('Save')}</PrimaryButton>
              </div>
            </form>
          </div>
        </div>
      </div>
    </AuthenticatedLayout>
  );
}
