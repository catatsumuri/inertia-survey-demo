import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head, useForm } from '@inertiajs/react';
import { useLaravelReactI18n } from 'laravel-react-i18n';
import InputError from '@/Components/InputError';
import InputLabel from '@/Components/InputLabel';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';

export default function SurveyCreate({ auth, surveyQuestionSets }) {
  const { t } = useLaravelReactI18n();
  const {
    data, setData, post, errors, processing,
  } = useForm({
    title: '',
    description: '',
    settings: '',
    survey_question_set_id: '',
  });

  const submit = (e) => {
    e.preventDefault();
    post(route('surveys.store'));
  };

  return (
    <AuthenticatedLayout
      user={auth.user}
      header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">{t('Create New Survey')}</h2>}
    >
      <Head title={t('Create New Survey')} />
      <div className="py-12">
        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
          <div className="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
            <form onSubmit={submit} className="mt-6 space-y-6">
              <div>
                <InputLabel htmlFor="title" value={t('Title')} />

                <TextInput
                  id="title"
                  className="mt-1 block w-full"
                  value={data.title}
                  onChange={(e) => setData('title', e.target.value)}
                  onSubmit={submit}
                  required
                />

                <InputError className="mt-2" message={errors.title} />
              </div>

              <div className="mt-3">
                <InputLabel htmlFor="description" value={t('Description')} />

                <TextInput
                  id="description"
                  className="mt-1 block w-full"
                  value={data.description}
                  onChange={(e) => setData('description', e.target.value)}
                  onSubmit={submit}
                />

                <InputError className="mt-2" message={errors.description} />
              </div>

              <div className="mt-3">
                <InputLabel htmlFor="survey_question_set_id" value={t('Question Set')} />
                <select
                  id="survey_question_set_id"
                  className="mt-1 block w-full"
                  value={data.survey_question_set_id}
                  onChange={(e) => setData('survey_question_set_id', e.target.value)}
                  required
                >
                  <option value="">
                    -- {t('Select a Question Set')} --
                  </option>

                  {Object.entries(surveyQuestionSets).map(([key, value]) => (
                    <option key={key} value={key}>
                      {value}
                    </option>
                  ))}
                </select>
                <InputError className="mt-2" message={errors.survey_question_set_id} />
              </div>

              <div className="flex items-center gap-4">
                <PrimaryButton disabled={processing}>{t('Save')}</PrimaryButton>
              </div>

            </form>
          </div>
        </div>
      </div>
    </AuthenticatedLayout>
  );
}
