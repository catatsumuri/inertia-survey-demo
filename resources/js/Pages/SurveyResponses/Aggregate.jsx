import React from 'react';
import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head, usePage } from '@inertiajs/react';
import SurveyResponseTabs from '@/Components/SurveyResponseTabs';

import surveyQuestions from '@/Components/AggregateViews/surveyQuestions';
const componentMap = {
  surveyQuestions,
};

export default function SurveyResponseAggregate({
  auth, surveyModel, surveyData, component
}) {
  const Aggregate = componentMap[component];

  return (
    <AuthenticatedLayout
      user={auth.user}
      header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">{surveyModel.title}</h2>}
    >
      <Head title={surveyModel.title} />
      <div className="py-12">
        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
          <div className="mb-4">
            <SurveyResponseTabs surveyModel={surveyModel} activeTab="aggregate" />
          </div>
           {Aggregate && <Aggregate />}
        </div>
      </div>
    </AuthenticatedLayout>
  );
}
