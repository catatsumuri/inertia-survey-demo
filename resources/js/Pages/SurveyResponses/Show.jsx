import React, { useCallback } from 'react';
import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Model } from 'survey-core';
import { Head, router, usePage } from '@inertiajs/react';
import SurveyWrapper from '@/Components/SurveyWrapper';
import SurveyResponseTabs from '@/Components/SurveyResponseTabs';

export default function SurveyResponseShow({
  auth, surveyModel, surveyData, responseData, isComponentAvailable,
}) {
  const { locale } = usePage().props;
  const survey = new Model(surveyData);
  const surveyComplete = useCallback((sender) => {
    router.post(route('surveys.previewStore', surveyModel.id), sender.data);
  }, []);
  survey.onComplete.add(surveyComplete);

  return (
    <AuthenticatedLayout
      user={auth.user}
      header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">{surveyModel.title}</h2>}
    >
      <Head title={surveyModel.title} />

      <div className="py-12">
        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">

          <div className="mb-4">
            <SurveyResponseTabs surveyModel={surveyModel} activeTab="view" isComponentAvailable={isComponentAvailable} />
          </div>

          <SurveyWrapper
            model={survey}
            readOnly
            responseData={responseData}
            locale={locale}
          />
        </div>
      </div>
    </AuthenticatedLayout>
  );
}
