import { useEffect } from 'react';
import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';

import { Head, Link, useForm } from '@inertiajs/react';
import { useLaravelReactI18n } from 'laravel-react-i18n';
import UserForm from './Partials/UserForm';

export default function UserCreate({ auth }) {
  const { t } = useLaravelReactI18n();
  const {
    data, setData, post, processing, errors, reset,
  } = useForm({
    name: '',
    email: '',
    password: '',
    password_confirmation: '',
  });

  useEffect(() => () => {
    reset('password', 'password_confirmation');
  }, []);

  const submit = (e) => {
    e.preventDefault();

    post(route('users.store'));
  };

  return (
    <AuthenticatedLayout
      user={auth.user}
      header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">{t('Create New User')}</h2>}
    >
      <Head title={t('Create New User')} />

      <div className="py-12">
        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
          <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg p-6">
            <UserForm
              data={data}
              setData={setData}
              onSubmit={submit}
              buttonLabel={t('Create New User')}
              errors={errors}
              processing={processing}
              isPasswordRequired
            />
          </div>
        </div>
      </div>
    </AuthenticatedLayout>
  );
}
