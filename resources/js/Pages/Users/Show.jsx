import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head, Link, router } from '@inertiajs/react';
import { VscGraphLine, VscEdit, VscTrash } from 'react-icons/vsc';
import { useLaravelReactI18n } from 'laravel-react-i18n';
import { useConfiguredDayjs } from '@/hooks/useConfiguredDayjs';

import RoleDisplay from '@/Components/RoleDisplay';
import EmailWithStatus from '@/Components/EmailWithStatus';
import LastLoginDisplay from '@/Components/LastLoginDisplay';
import PrimaryButton from '@/Components/PrimaryButton';
import SecondaryButton from '@/Components/SecondaryButton';
import DangerButton from '@/Components/DangerButton';

export default function UserShow({ auth, user }) {
  const { t } = useLaravelReactI18n();
  const dayjs = useConfiguredDayjs();

  const handleDelete = (id) => {
    if (confirm(t('Are you sure you want to delete your account?'))) {
      router.delete(route('users.destroy', id));
    }
  };

  return (
    <AuthenticatedLayout
      user={auth.user}
      header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">{t('User Details')}</h2>}
    >
      <Head title={t('User Details')} />

      <div className="py-12">
        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
          <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg p-4">
            <div className="mt-6 space-y-6">
              <div>
                <h3 className="text-lg font-medium leading-6 text-gray-900">ID</h3>
                <p className="mt-2 text-lg text-gray-500">{user.id}</p>
              </div>

              <div>
                <h3 className="text-lg font-medium leading-6 text-gray-900">{t('Email')}</h3>
                <div className="mt-2 flex items-center text-lg text-gray-500">
                  <EmailWithStatus email={user.email} isVerified={user.email_verified_at !== null} />
                </div>
              </div>

              <div>
                <h3 className="text-lg font-medium leading-6 text-gray-900">{t('Name')}</h3>
                <p className="mt-2 text-lg text-gray-500">{user.name}</p>
              </div>

              <div>
                <h3 className="text-lg font-medium leading-6 text-gray-900">{t('Roles')}</h3>
                <p className="mt-2">
                  <RoleDisplay roles={user.roles} />
                </p>
              </div>

              <div>
                <h3 className="text-lg font-medium leading-6 text-gray-900">{t('Last Login At')}</h3>
                <p className="mt-2 text-lg text-gray-500">
                  <LastLoginDisplay lastLoginAt={user.last_login_at} />
                </p>

                <div>
                  <SecondaryButton
                    href={route('users.activity-log', user.id)}
                    disabled={!user.activities || user.activities.length === 0}
                  >
                    <VscGraphLine className="mr-2" />
                    {t('View Activity Log')}
                  </SecondaryButton>
                </div>
              </div>

              <div className="mt-6 flex justify-end space-x-4">
                <PrimaryButton href={route('users.edit', user.id)}>
                  <VscEdit className="mr-2" />
                  {t('Edit')}
                </PrimaryButton>
                <DangerButton onClick={() => handleDelete(user.id)}>
                  <VscTrash className="mr-2" />
                  {t('Delete Account')}
                </DangerButton>
              </div>
            </div>
          </div>
        </div>
      </div>
    </AuthenticatedLayout>
  );
}
