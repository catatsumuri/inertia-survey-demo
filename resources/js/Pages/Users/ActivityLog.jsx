import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head, Link } from '@inertiajs/react';
import { useLaravelReactI18n } from 'laravel-react-i18n';
import { useConfiguredDayjs } from '@/hooks/useConfiguredDayjs';
import PaginationNav from '@/Components/PaginationNav';

export default function ActivityLog({ auth, user, activities }) {
  const { t } = useLaravelReactI18n();
  const dayjs = useConfiguredDayjs();

  return (
    <AuthenticatedLayout
      user={auth.user}
      header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">{t('View Activity Log')}</h2>}
    >
      <Head title={t('View Activity Log')} />

      <div className="py-12">
        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
          <div className="bg-white overflow-auto shadow-sm sm:rounded-lg p-4">
            <div className="mt-6">
              <PaginationNav links={activities.links} />

              {activities.data && activities.data.length > 0 ? (
                <table className="min-w-full divide-y divide-gray-200">
                  <thead className="bg-gray-50">
                    <tr>
                      <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">ID</th>
                      <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">{t('Description')}</th>
                      <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">{t('Created At')}</th>
                      <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">{t('Properties')}</th>
                    </tr>
                  </thead>
                  <tbody className="bg-white divide-y divide-gray-200">
                    {activities.data.map((activity, index) => (
                      <tr key={index}>
                        <td className="px-6 py-4 whitespace-nowrap">{activity.id}</td>
                        <td className="px-6 py-4 whitespace-nowrap">{activity.description}</td>
                        <td className="px-6 py-4 whitespace-nowrap">
                          <span>{dayjs(activity.created_at).format('YYYY-MM-DD HH:mm:ss')}</span>
                          <small className="ml-2 text-sm text-gray-600">
                            (
                            {dayjs(activity.created_at).fromNow()}
                            )
                          </small>
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap">
                          <table className="min-w-full divide-y divide-gray-200">
                            <tbody className="bg-white divide-y divide-gray-200">
                              {Object.entries(activity.properties).map(([key, value], idx) => (
                                <tr key={idx}>
                                  <th className="px-2 py-1 text-sm text-gray-500">{key}</th>
                                  <td className="px-2 py-1 text-sm text-gray-900">{value}</td>
                                </tr>
                              ))}
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              ) : (
                <p className="text-gray-500">{t('No activities recorded.')}</p>
              )}
            </div>
          </div>
        </div>
      </div>
    </AuthenticatedLayout>
  );
}
