import InputLabel from '@/Components/InputLabel';
import TextInput from '@/Components/TextInput';
import InputError from '@/Components/InputError';
import PrimaryButton from '@/Components/PrimaryButton';
import { useLaravelReactI18n } from 'laravel-react-i18n';

export default function UserForm({
  data, setData, onSubmit, buttonLabel, errors, processing, isPasswordRequired,
}) {
  const { t } = useLaravelReactI18n();

  return (
    <form onSubmit={onSubmit} className="space-y-4">
      <div>
        <InputLabel htmlFor="name" value={t('Name')} />
        <TextInput
          id="name"
          name="name"
          value={data.name}
          className="mt-1 block md:w-1/2 w-full"
          onChange={(e) => setData('name', e.target.value)}
          required
        />
        <InputError message={errors.name} className="mt-2" />
      </div>

      <div className="mt-4">
        <InputLabel htmlFor="email" value={t('Email')} />

        <TextInput
          id="email"
          type="email"
          name="email"
          value={data.email}
          className="mt-1 block md:w-1/2 w-full"
          onChange={(e) => setData('email', e.target.value)}
          required
        />

        <InputError message={errors.email} className="mt-2" />
      </div>

      <div className="mt-4">
        <InputLabel htmlFor="password" value={t('Password')} />

        <TextInput
          id="password"
          type="password"
          name="password"
          value={data.password}
          className="mt-1 block sm:w-1/4 w-full"
          autoComplete="new-password"
          onChange={(e) => setData('password', e.target.value)}
          required={isPasswordRequired}
        />

        <InputError message={errors.password} className="mt-2" />
      </div>

      <div className="mt-4">
        <InputLabel htmlFor="password_confirmation" value={t('Confirm Password')} />

        <TextInput
          id="password_confirmation"
          type="password"
          name="password_confirmation"
          value={data.password_confirmation}
          className="mt-1 block sm:w-1/4 w-full"
          autoComplete="new-password"
          onChange={(e) => setData('password_confirmation', e.target.value)}
          required={isPasswordRequired}
        />

        <InputError message={errors.password_confirmation} className="mt-2" />
      </div>

      <PrimaryButton className="ml-4" disabled={processing}>
        {buttonLabel}
      </PrimaryButton>
    </form>
  );
}
