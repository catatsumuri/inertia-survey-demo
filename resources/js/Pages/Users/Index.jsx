import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head, router } from '@inertiajs/react';
import { useLaravelReactI18n } from 'laravel-react-i18n';
import Dropdown from '@/Components/Dropdown';
import PrimaryButton from '@/Components/PrimaryButton';
import PaginationNav from '@/Components/PaginationNav';
import RoleDisplay from '@/Components/RoleDisplay';
import AppLink from '@/Components/AppLink';
import EmailWithStatus from '@/Components/EmailWithStatus';
import LastLoginDisplay from '@/Components/LastLoginDisplay';
import {
  VscAdd,
  VscEllipsis,
} from 'react-icons/vsc';

export default function UserIndex({ auth, users }) {
  const { t } = useLaravelReactI18n();

  const handleDelete = (id) => {
    if (confirm(t('Are you sure you want to delete your account?'))) {
      router.delete(route('users.destroy', id));
    }
  };

  return (
    <AuthenticatedLayout
      user={auth.user}
      header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">{t('Users')}</h2>}
    >
      <Head title="Users" />

      <div className="py-12">
        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
          <div className="bg-white overflow-auto shadow-sm sm:rounded-lg">
            <div className="p-6 bg-white border-b border-gray-200 flex justify-end">
              <PrimaryButton href={route('users.create')}>
                <VscAdd className="mr-2" />
                {t('Create New User')}
              </PrimaryButton>
            </div>
            <PaginationNav links={users.links} />

            <table className="min-w-full divide-y divide-gray-200 m-3 mb-20">
              <thead className="bg-gray-50">
                <tr>
                  <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    {t('ID')}
                  </th>
                  <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    {t('Email')}
                  </th>
                  <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    {t('Name')}
                  </th>
                  <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    {t('Roles')}
                  </th>
                  <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    {t('Last Login')}
                  </th>
                  <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    {t('Action')}
                  </th>
                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-200">
                {users.data.map((user, index) => (
                  <tr key={index}>
                    <td className="px-6 py-4 whitespace-nowrap">
                      {user.id}
                    </td>

                    <td className="px-6 py-4 whitespace-nowrap flex items-center">
                      <AppLink href={route('users.show', user.id)}>
                        <EmailWithStatus
                          email={user.email}
                          isVerified={user.email_verified_at !== null}
                        />
                      </AppLink>
                    </td>

                    <td className="px-6 py-4 whitespace-nowrap">
                      {user.name}
                    </td>

                    <td className="px-6 py-4 whitespace-nowrap">
                      <RoleDisplay roles={user.roles} />
                    </td>

                    <td className="px-6 py-4 whitespace-nowrap">
                      <LastLoginDisplay lastLoginAt={user.last_login_at} />
                    </td>

                    <td className="px-6 py-4 whitespace-nowrap">
                      <Dropdown>
                        <Dropdown.Trigger>
                          <button type="button">
                            <VscEllipsis />
                          </button>
                        </Dropdown.Trigger>
                        <Dropdown.Content>
                          <Dropdown.Link href={route('users.edit', user.id)}>
                            {t('Edit')}
                          </Dropdown.Link>
                          <Dropdown.Button onClick={() => handleDelete(user.id)}>
                            {t('Delete Account')}
                          </Dropdown.Button>
                        </Dropdown.Content>
                      </Dropdown>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </AuthenticatedLayout>
  );
}
