import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head } from '@inertiajs/react';
import { useLaravelReactI18n } from 'laravel-react-i18n';
import PrimaryButton from '@/Components/PrimaryButton';
import SecondaryButton from '@/Components/SecondaryButton';
import { useConfiguredDayjs } from '@/hooks/useConfiguredDayjs';

import {
  VscChecklist,
  VscEye,
} from 'react-icons/vsc';

export default function Dashboard({ auth, surveys }) {
  const { t } = useLaravelReactI18n();
  const dayjs = useConfiguredDayjs();

  return (
    <AuthenticatedLayout
      user={auth.user}
      header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">{t('Dashboard')}</h2>}
    >
      <Head title={t('Dashboard')} />

      <div className="py-12">
        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
          <div className="bg-white p-6 rounded shadow-md max-w-7xl mx-auto">
            <div className="flex justify-between items-center mb-4">
              <h3 className="text-2xl font-semibold">
                {t('Available Surveys')}
              </h3>
            </div>

            <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-4">
              {surveys.map((survey, index) => (
                <div key={index} className="bg-white rounded-lg shadow-lg p-4 border border-gray-300">
                  <div className="flex justify-between items-center mb-2">
                    <h4 className="text-lg font-semibold">
                      {survey.title}
                    </h4>
                  </div>

                  <p className="text-sm text-gray-700 mb-3">
                    {survey.description}
                  </p>

                  {survey.response_id ? (
                    <>
                      <SecondaryButton href={route('surveys.responses.show', { survey: survey.id, response: survey.response_id })}>
                        <VscEye className="mr-2" /> {t('Show Details')}
                      </SecondaryButton>
                      <p className="text-sm text-gray-700 mt-2 p-2 border border-blue-300 bg-blue-100 rounded">
                        {t('Submitted At')}:
                        {dayjs(survey.response_created_at).format('YYYY/MM/DD HH:mm:ss')}
                        <small className="ml-2">({dayjs(survey.response_created_at).fromNow()})</small>
                      </p>
                    </>
                  ) : (
                    <PrimaryButton href={route('surveys.take', survey.id)}>
                      <VscChecklist className="mr-2" /> {t('Take The Survey')}
                    </PrimaryButton>
                  )}
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </AuthenticatedLayout>
  );
}
