import { Link } from '@inertiajs/react';
import { useLaravelReactI18n } from 'laravel-react-i18n';

export default function SurveyResponseTabs({ surveyModel, activeTab, isComponentAvailable }) {
  const { t } = useLaravelReactI18n();
  return (
    <nav className="flex">
      <Link
        href={route('surveys.responses.show', { survey: surveyModel.id, response: surveyModel.response_id })}
        className={`py-2 px-4 rounded-t-lg border border-gray-300 ${activeTab === 'view' ? 'bg-blue-500 text-white hover:bg-blue-700' : 'bg-gray-200 text-gray-500 hover:bg-gray-300'}`}
      >
        {t("Response View")}
      </Link>
      {isComponentAvailable && (
        <Link
          href={route('surveys.responses.aggregate', { survey: surveyModel.id, response: surveyModel.response_id })}
          className={`py-2 px-4 rounded-t-lg border-b-0 ${activeTab === 'aggregate' ? 'bg-blue-500 text-white hover:bg-blue-700' : 'bg-gray-200 text-gray-500 hover:bg-gray-300'}`}
        >
          {t("Aggregate View")}
        </Link>
      )}
    </nav>
  );
}
