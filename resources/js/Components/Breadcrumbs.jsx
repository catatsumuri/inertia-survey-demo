import React from 'react';
import { usePage } from '@inertiajs/react';
import { AiOutlineDoubleRight } from 'react-icons/ai';

export default function Breadcrumbs() {
  const { breadcrumbs } = usePage().props;

  if (!breadcrumbs) {
    return null;
  }

  return (
    <div className="border-t border-gray-200 py-2">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <ol className="flex space-x-2">
          {breadcrumbs.map((breadcrumb, index, arr) => (
            <React.Fragment key={index}>
              <li>
                <a href={breadcrumb.url} className={breadcrumb.current ? 'border-b-2 border-blue-400' : ''}>
                  {breadcrumb.title}
                </a>
              </li>
              {index < arr.length - 1 && <li className="flex items-center text-gray-500"><AiOutlineDoubleRight className="mx-1" size={12} /></li>}
            </React.Fragment>
          ))}
        </ol>
      </div>
    </div>
  );
}
