import { useLaravelReactI18n } from 'laravel-react-i18n';
import { useConfiguredDayjs } from '@/hooks/useConfiguredDayjs';

function LastLoginDisplay({ lastLoginAt }) {
  const { t } = useLaravelReactI18n();
  const dayjs = useConfiguredDayjs();

  return (
    lastLoginAt ? (
      <small>
        <span>{dayjs(lastLoginAt).format('YY-MM-DD HH:mm:ss')}</span>
        <small className="ml-2 text-gray-600">
          ( {dayjs(lastLoginAt).fromNow()} )
        </small>
      </small>
    ) : (
      <span className="text-gray-500">{t('Never')}</span>
    )
  );
}

export default LastLoginDisplay;
