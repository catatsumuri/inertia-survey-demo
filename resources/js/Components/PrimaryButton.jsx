import { Link } from '@inertiajs/react';

export default function PrimaryButton({
  type = 'submit',
  className = '',
  disabled,
  children,
  href,
  ...props
}) {
  const buttonClass = `inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 transition ease-in-out duration-150 ${disabled ? 'opacity-25 cursor-not-allowed' : ''} ${className}`;

  const handleClick = (e) => {
    if (disabled) {
      e.preventDefault();
    }
  };

  if (href) {
    return (
      <Link
        href={href}
        className={buttonClass}
        onClick={handleClick}
        {...props}
      >
        {children}
      </Link>
    );
  }

  return (
    <button
      type={type}
      {...props}
      className={buttonClass}
      disabled={disabled}
    >
      {children}
    </button>
  );
}
