import { Link } from '@inertiajs/react';

export default function PaginationNav({ links }) {
  if (!links) return null;

  return (
    <nav className="flex justify-end space-x-2 my-2">
      {links.map((link, index) => (
        <Link
          key={index}
          href={link.url || '#'}
          className={
                        link.active
                          ? 'px-2 py-1 bg-blue-500 text-white border border-blue-500 rounded text-sm'
                          : 'px-2 py-1 hover:underline border border-transparent rounded hover:border-gray-300 text-xs'
                    }
        >
          <span dangerouslySetInnerHTML={{ __html: link.label }} />
        </Link>
      ))}
    </nav>
  );
}
