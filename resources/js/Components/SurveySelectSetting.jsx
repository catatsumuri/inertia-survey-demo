import InputLabel from '@/Components/InputLabel';

export default function SurveySelectSetting({
  settingKey, value, onChange, options, label,
}) {
  return (
    <>
      <InputLabel htmlFor={settingKey} value={label || settingKey} />
      <select
        id={settingKey}
        value={value || 'standard'}
        onChange={(e) => onChange(settingKey, e.target.value)}
      >
        {options.map((option) => (
          <option key={option} value={option}>
            {option}
          </option>
        ))}
      </select>
    </>
  );
}
