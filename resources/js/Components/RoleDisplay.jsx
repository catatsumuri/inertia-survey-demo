import { useLaravelReactI18n } from 'laravel-react-i18n';

export default function RoleDisplay({ roles }) {
  const { t } = useLaravelReactI18n();
  if (!roles || roles.length === 0) {
    return (
      <span className="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-gray-200 text-gray-600">
        {t('No roles')}
      </span>
    );
  }

  return (
    <>
      {roles.map((role, index) => (
        <span
          key={index}
          className="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-blue-100 text-blue-800"
        >
          {role.name}
        </span>
      ))}
    </>
  );
}
