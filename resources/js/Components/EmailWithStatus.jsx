import { VscVerifiedFilled, VscUnverified } from 'react-icons/vsc';

export default function EmailWithStatus({ email, isVerified }) {
  return (
    <div className="flex items-center text-gray-500">
      {isVerified ? (
        <VscVerifiedFilled className="text-green-500 mr-2 text-xl" />
      ) : (
        <VscUnverified className="text-red-500 mr-2 text-lg" />
      )}
      <span>{email}</span>
    </div>
  );
}
