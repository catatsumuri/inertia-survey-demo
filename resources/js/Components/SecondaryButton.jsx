import { Link } from '@inertiajs/react';

export default function SecondaryButton({
  type = 'submit',
  className = '',
  disabled,
  children,
  href,
  ...props
}) {
  const buttonClass = `inline-flex items-center px-4 py-2 bg-white border border-gray-300 rounded-md font-semibold text-xs text-gray-700 uppercase tracking-widest shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 ${disabled ? 'opacity-25 cursor-not-allowed' : ''} ${className}`;

  const handleClick = (e) => {
    if (disabled) {
      e.preventDefault();
    }
  };

  if (href) {
    return (
      <Link
        href={href}
        className={buttonClass}
        onClick={handleClick}
        {...props}
      >
        {children}
      </Link>
    );
  }

  return (
    <button
      {...props}
      type={type}
      className={buttonClass}
      disabled={disabled}
      onClick={handleClick}
    >
      {children}
    </button>
  );
}
