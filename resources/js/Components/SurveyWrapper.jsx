import React from 'react';
import { Survey } from 'survey-react-ui';
import 'survey-core/defaultV2.min.css';
import 'survey-core/survey.i18n';

export default function SurveyWrapper({
  model, readOnly, responseData, locale,
}) {
  if (readOnly && responseData) {
    model.mode = 'display';
    model.data = responseData;
  }

  if (locale) {
    model.locale = locale;
  }

  return (
    <Survey model={model} />
  );
}
